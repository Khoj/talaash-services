package com.talaash;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

@SpringBootApplication
public class TalaashServicesApplication extends SpringBootServletInitializer {


    public static void main(String[] args) {
        SpringApplication.run(TalaashServicesApplication.class, args);
//        FirebaseOptions options = null;
//        try {
//            options = new FirebaseOptions.Builder()
//                    .setServiceAccount(new FileInputStream("talaash-services.json"))
//                    .setDatabaseUrl("https://talaash-2e4b2.firebaseio.com/")
//                    .build();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }
//        FirebaseApp.initializeApp(options);

    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(TalaashServicesApplication.class);
    }
}
