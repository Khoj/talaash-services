package com.talaash.translator;

import com.talaash.model.FoundCase;
import com.talaash.model.MissingCase;
import com.talaash.vo.CaseResponse;
import com.talaash.vo.StatusResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imittal on 5/21/16.
 */
@Component
public class MyCaseTranslator implements Translator<CaseResponse, List<MissingCase>, List<FoundCase>> {

    @Override
    public CaseResponse translate(List<MissingCase> obj, List<FoundCase> obj1) {
        CaseResponse caseResponse = new CaseResponse();
        MissingCase missingCase = obj.get(0);
        caseResponse.setAge(missingCase.getAge());
        caseResponse.setHeight(missingCase.getHeight());
        caseResponse.setIdentificationMarkOne(missingCase.getIdentificationMarkOne());
        caseResponse.setIdentificationMarkTwo(missingCase.getIdentificationMarkTwo());
        caseResponse.setCaseId(String.valueOf(missingCase.getId()));
        caseResponse.setName(missingCase.getName());
        caseResponse.setSex(missingCase.getGender());
        caseResponse.setImagePaths(Utils.getListOfMissingCaseImagesPathsForClient(1, missingCase.getUser().getUserToken()));
        caseResponse.setHeightUnit(missingCase.getHeightUnit());
        caseResponse.setStatusList(convertFoundListToStatusList(obj1));
        return caseResponse;
    }

    private List<StatusResponse> convertFoundListToStatusList(List<FoundCase> obj1) {
        List<StatusResponse> statusResponseList = new ArrayList<>();
        for (FoundCase foundCase : obj1) {
            StatusResponse status = new StatusResponse();
            status.setDate(foundCase.getDate().toString());
            status.setPlace(foundCase.getLastLocatedAt());
            status.setReportedBy(foundCase.getUser().getFullName());
            status.setTime(foundCase.getTime().toString());
            statusResponseList.add(status);
        }
        return statusResponseList;
    }


}
