package com.talaash.translator;

import com.talaash.model.FoundCase;
import com.talaash.vo.ReportFoundRequest;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by imittal on 5/22/16.
 */
@Component
public class ReportFoundCaseTranslator implements Translator<FoundCase, ReportFoundRequest, String> {
    @Override
    public FoundCase translate(ReportFoundRequest obj, String obj1) {
        FoundCase foundCase = new FoundCase();
        foundCase.setName(obj.getName());
        foundCase.setIdentificationMarkOne(obj.getIdentificationMarkOne());
        foundCase.setHeightUnit(obj.getHeightUnit());
        foundCase.setLastLocatedAt(obj.getLastLocatedAt());
        foundCase.setIdentificationMarkTwo(obj.getIdentificationMarkTwo());
        foundCase.setAge(obj.getApproxAge());
        foundCase.setHeight(obj.getApproxHeight());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date parsed = formatter.parse(obj.getDate());
            foundCase.setDate(new java.sql.Date(parsed.getTime()));
            SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm");
            Date parsedDate = dateFormat.parse(obj.getTime());
            foundCase.setTime(new java.sql.Timestamp(parsedDate.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        foundCase.setDetails(obj.getOtherDetails());
        foundCase.setGender(obj.getGender());
        foundCase.setUser(obj.getUser());
        return foundCase;
    }
}
