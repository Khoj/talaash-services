package com.talaash.translator;

import com.talaash.model.FoundCase;
import com.talaash.vo.MissingAndFoundResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imittal on 5/21/16.
 */
@Component

public class FoundCaseTranslator implements Translator<List<MissingAndFoundResponse>, Iterable<FoundCase>, String> {
    @Override
    public List<MissingAndFoundResponse> translate(Iterable<FoundCase> obj, String obj1) {
        List<MissingAndFoundResponse> listOfFoundCases = new ArrayList<>();
        for (FoundCase foundCase : obj) {
            MissingAndFoundResponse missingAndFoundResponse = new MissingAndFoundResponse();
            missingAndFoundResponse.setIdentificationMarkOne(foundCase.getIdentificationMarkOne());
            missingAndFoundResponse.setSex(foundCase.getGender());
            missingAndFoundResponse.setAge(foundCase.getAge());
            missingAndFoundResponse.setIdentificationMarkTwo(foundCase.getIdentificationMarkTwo());
            missingAndFoundResponse.setLastLocatedAt(foundCase.getLastLocatedAt());
            missingAndFoundResponse.setName(foundCase.getName());
            List<String> image = Utils.getListOfFoundCaseImagesPathsForClient(1, foundCase.getUser().getUserToken());
            missingAndFoundResponse.setImagePath(image.get(0));
            missingAndFoundResponse.setCaseId(foundCase.getUser().getId());
            listOfFoundCases.add(missingAndFoundResponse);
        }
        return listOfFoundCases;
    }
}
