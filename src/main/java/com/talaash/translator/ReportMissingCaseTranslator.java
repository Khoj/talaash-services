package com.talaash.translator;

import com.talaash.model.MissingCase;
import com.talaash.vo.MissingCaseRequest;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by imittal on 5/22/16.
 */
@Component
public class ReportMissingCaseTranslator implements Translator<MissingCase, MissingCaseRequest, String> {
    @Override
    public MissingCase translate(MissingCaseRequest obj, String obj1) {
        MissingCase missingCase = new MissingCase();
        missingCase.setName(obj.getName());
        missingCase.setIdentificationMarkOne(obj.getIdentificationMarkOne());
        missingCase.setHeightUnit(obj.getHeightUnit());
        missingCase.setLastLocatedAt(obj.getLastLocatedAt());
        missingCase.setIdentificationMarkTwo(obj.getIdentificationMarkTwo());
        missingCase.setAge(obj.getAge());
        missingCase.setHeight(obj.getHeight());
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date parsed = formatter.parse(obj.getDate());
            missingCase.setDate(new java.sql.Date(parsed.getTime()));
            SimpleDateFormat dateFormat = new SimpleDateFormat("h:mm");
            Date parsedDate = dateFormat.parse(obj.getTime());
            missingCase.setTime(new java.sql.Timestamp(parsedDate.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        missingCase.setDetails(obj.getDetails());
        missingCase.setGender(obj.getGender());
        missingCase.setLanguageFour(obj.getLanguageFour());
        missingCase.setLanguageThree(obj.getLanguageThree());
        missingCase.setLanguageTwo(obj.getLanguageTwo());
        missingCase.setLanguageOne(obj.getLanguageOne());
        missingCase.setNumberOfPics(obj.getNumberOfPics());
        missingCase.setOtherDetails(obj.getOtherDetails());
        missingCase.setRelationToMissingPerson(obj.getRelationToMissingPerson());
        missingCase.setStatusOfCase(obj.getStatusOfCase());
        missingCase.setUser(obj.getUser());
        return missingCase;
    }
}
