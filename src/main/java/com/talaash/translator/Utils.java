package com.talaash.translator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imittal on 5/21/16.
 */
public class Utils {

    private static final String LOCAL_BASE_URL = "/var/lib/tomcat7/webapps/images";
    private static final String MISSING_CASE_DIRECTORY = "missing";
    private static final String FOUND_CASE_DIRECTORY = "found";
    private static final String IMG_EXTENSION = ".jpeg";
    private static final String IMAGE_BASE_URL = "http://ec2-52-40-83-227.us-west-2.compute.amazonaws.com:8080/images";

    public static List<String> setListOfMissingCaseImagesPaths(int noOfImages, String userToken) {
        List<String> imagePaths = new ArrayList<>();
        for (int i = 1; i <= noOfImages; i++) {
            imagePaths.add(LOCAL_BASE_URL + "/" + MISSING_CASE_DIRECTORY + "/" + userToken + "/" + i + IMG_EXTENSION);
        }
        return imagePaths;
    }

    public static List<String> setListOfFoundCaseImagesPaths(int noOfImages, String userToken) {
        List<String> imagePaths = new ArrayList<>();
        for (int i = 1; i <= noOfImages; i++) {
            imagePaths.add(LOCAL_BASE_URL + "/" + FOUND_CASE_DIRECTORY + "/" + userToken + "/" + i + IMG_EXTENSION);
        }
        return imagePaths;
    }

    public static List<String> getListOfMissingCaseImagesPathsForClient(int noOfImages, String userToken) {
        List<String> imagePaths = new ArrayList<>();
        for (int i = 1; i <= noOfImages; i++) {
            imagePaths.add(IMAGE_BASE_URL + "/" + MISSING_CASE_DIRECTORY + "/" + userToken + "/" + i + IMG_EXTENSION);
        }
        return imagePaths;
    }

    public static List<String> getListOfFoundCaseImagesPathsForClient(int noOfImages, String userToken) {
        List<String> imagePaths = new ArrayList<>();
        for (int i = 1; i <= noOfImages; i++) {
            imagePaths.add(IMAGE_BASE_URL + "/" + FOUND_CASE_DIRECTORY + "/" + userToken + "/" + i + IMG_EXTENSION);
        }
        return imagePaths;
    }


}
