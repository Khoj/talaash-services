package com.talaash.translator;

/**
 * Created by imittal on 5/21/16.
 */
public interface Translator<K, T, V> {
    K translate(T obj, V obj1);
}
