package com.talaash.translator;

import com.talaash.model.MissingCase;
import com.talaash.vo.MissingAndFoundResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by imittal on 5/21/16.
 */
@Component
public class MissingCaseTranslator implements Translator<List<MissingAndFoundResponse>, Iterable<MissingCase>, String> {
    @Override
    public List<MissingAndFoundResponse> translate(Iterable<MissingCase> obj, String obj1) {
        List<MissingAndFoundResponse> listOfMissingCases = new ArrayList<>();
        for (MissingCase missingCase : obj) {
            MissingAndFoundResponse missingAndFoundResponse = new MissingAndFoundResponse();
            missingAndFoundResponse.setIdentificationMarkOne(missingCase.getIdentificationMarkOne());
            missingAndFoundResponse.setSex(missingCase.getGender());
            missingAndFoundResponse.setAge(missingCase.getAge());
            missingAndFoundResponse.setIdentificationMarkTwo(missingCase.getIdentificationMarkTwo());
            missingAndFoundResponse.setLastLocatedAt(missingCase.getLastLocatedAt());
            missingAndFoundResponse.setName(missingCase.getName());
            List<String> images = Utils.getListOfMissingCaseImagesPathsForClient(1, missingCase.getUser().getUserToken());
            missingAndFoundResponse.setImagePath(images.get(0));
            missingAndFoundResponse.setCaseId(missingCase.getUser().getId());
            System.out.println(images.get(0));
            listOfMissingCases.add(missingAndFoundResponse);
        }
        return listOfMissingCases;
    }
}
