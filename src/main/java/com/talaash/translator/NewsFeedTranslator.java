package com.talaash.translator;

import com.talaash.vo.Datum;
import com.talaash.vo.Example;
import com.talaash.vo.FeedResponse;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subha_000 on 04-06-2016.
 */
@Component
public class NewsFeedTranslator implements Translator<List<FeedResponse>, Example, String> {
    @Override
    public List<FeedResponse> translate(Example response, String obj1) {
        List<FeedResponse> feedResponseList = new ArrayList<>();
        for (Datum datum : response.getData()) {
            FeedResponse feedResponse = new FeedResponse();
            feedResponse.setDescription(datum.getDescription());
            feedResponse.setImageUrl(datum.getFullPicture());
            feedResponse.setName(datum.getName());
            feedResponse.setPostedOn(datum.getCreatedTime());
            feedResponseList.add(feedResponse);
        }
        return feedResponseList;
    }
}
