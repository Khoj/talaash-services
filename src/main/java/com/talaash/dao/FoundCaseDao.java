package com.talaash.dao;

import com.talaash.model.FoundCase;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by imittal on 5/18/16.
 */
@Transactional
public interface FoundCaseDao extends CrudRepository<FoundCase, Integer> {

}
