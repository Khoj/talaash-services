package com.talaash.dao;

import com.talaash.model.MissingCase;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by imittal on 5/18/16.
 */
@Transactional
public interface MissingCaseDao extends CrudRepository<MissingCase, Integer> {

    @Query("select missingCase from MissingCase missingCase where missingCase.user.userToken=?1")
    List<MissingCase> getMyCase(String userToken);

    @Modifying
    @Query("Update MissingCase missingCase SET missingCase.statusOfCase =?1 WHERE missingCase.user.userToken=?2")
    void updateCaseStatus(Boolean status, String userToken);

    List<MissingCase> findById(int id);
}
