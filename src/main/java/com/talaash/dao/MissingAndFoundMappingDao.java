package com.talaash.dao;


import com.talaash.model.FoundCase;
import com.talaash.model.MissingAndFoundMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by imittal on 5/20/16.
 */
@Transactional
public interface MissingAndFoundMappingDao extends CrudRepository<MissingAndFoundMapping, Integer> {

    @Query("select mapping.foundCase from MissingAndFoundMapping mapping where mapping.missingCase.user.userToken=?1")
    List<FoundCase> getMyCaseStatuses(String userToken);

    @Query("select mapping.foundCase from MissingAndFoundMapping mapping where mapping.missingCase.user.id=?1")
    List<FoundCase> getCaseStatuses(int id);
}
