package com.talaash.dao;

import com.talaash.model.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by imittal on 5/15/16.
 */
@Transactional
public interface UserDao extends CrudRepository<User, Integer>, JpaSpecificationExecutor<User> {

    User findByUserToken(String userToken);

}
