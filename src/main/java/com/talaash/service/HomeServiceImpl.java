package com.talaash.service;

import com.talaash.dao.FoundCaseDao;
import com.talaash.dao.MissingCaseDao;
import com.talaash.interfaces.HomeService;
import com.talaash.model.FoundCase;
import com.talaash.model.MissingCase;
import com.talaash.translator.Translator;
import com.talaash.vo.MissingAndFoundResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by imittal on 5/17/16.
 */
@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    MissingCaseDao missingCaseDao;

    @Autowired
    FoundCaseDao foundCaseDao;

    @Autowired
    private Translator<List<MissingAndFoundResponse>, Iterable<MissingCase>, String> missingCaseTranslator;

    @Autowired
    private Translator<List<MissingAndFoundResponse>, Iterable<FoundCase>, String> foundCaseTranslator;

    @Override
    public List<MissingAndFoundResponse> getMissingPeople(String userToken) {
        // System.out.println(missingCaseDao.findAll().toString());
        return missingCaseTranslator.translate(missingCaseDao.findAll(), userToken);
    }

    @Override
    public List<MissingAndFoundResponse> getFoundPeople(String userToken) {
        return foundCaseTranslator.translate(foundCaseDao.findAll(), userToken);
    }
}
