package com.talaash.service;

import com.talaash.dao.UserDao;
import com.talaash.interfaces.SignUpService;
import com.talaash.model.User;
import com.talaash.vo.GenerateOtpResponse;
import com.talaash.vo.OtpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Random;
import java.util.UUID;

/**
 * Created by imittal on 5/15/16.
 */
@Service
public class SignUpServiceImpl implements SignUpService {

    @Autowired
    UserDao userDao;

    private String GetOtp() {
        return String.valueOf((new Random().nextInt((9999 - 1000) + 1) + 1000));
    }

    @Override
    public GenerateOtpResponse generateOtp(String phoneNumber, String username) {
        // TODO: check how to generate otp uing algorithm and send sms
        byte[] key = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        // Otp.Totp totp = new Otp.Totp(30, key);
        for (User user : userDao.findAll()) {
            if (user.getUserName().equals(username)) {
                return new GenerateOtpResponse("Please Choose another user name", true);
            }
        }
        String otp = GetOtp();
        SendSMS(phoneNumber, otp, username);
        return new GenerateOtpResponse(otp, false);
    }

    private void SendSMS(String phoneNumber, String otp, String username) {

        //Your authentication key
        String authkey = "113548A3j4iNTW3573df1d2";
//Multiple mobiles numbers separated by comma
        String mobiles = phoneNumber;
//Sender ID,While using route4 sender id should be 6 characters long.
        String senderId = "TALAAS";
//Your message to send, Add URL encoding here.
        String message = String.format("Dear %s\nYour verification code for Talaash is %s.", username, otp);
//define route
        String route = "4";

//Prepare Url
        URLConnection myURLConnection = null;
        URL myURL = null;
        BufferedReader reader = null;

//encoding message
        String encoded_message = URLEncoder.encode(message);

//Send SMS API
        String mainUrl = "https://control.msg91.com/api/sendhttp.php?";

//Prepare parameter string
        StringBuilder sbPostData = new StringBuilder(mainUrl);
        sbPostData.append("authkey=" + authkey);
        sbPostData.append("&mobiles=" + mobiles);
        sbPostData.append("&message=" + encoded_message);
        sbPostData.append("&route=" + route);
        sbPostData.append("&sender=" + senderId);

//final string
        mainUrl = sbPostData.toString();
        try {
            //prepare connection
            myURL = new URL(mainUrl);
            myURLConnection = myURL.openConnection();
            myURLConnection.connect();
            reader = new BufferedReader(new InputStreamReader(myURLConnection.getInputStream()));
            //reading response
            String response;
            while ((response = reader.readLine()) != null)
                //print response
                System.out.println(response);

            //finally close connection
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public OtpResponse verifyOtp(String userName, String password, String phoneNumber, String correctOtp, String enteredOtp) {
        if (correctOtp.equals(enteredOtp)) {
            UUID userToken = UUID.randomUUID();
            userDao.save(new User(userName, password, phoneNumber, userToken.toString(), ""));
            return new OtpResponse(userToken.toString(), false);
        }

        return new OtpResponse("", true);
    }


}
