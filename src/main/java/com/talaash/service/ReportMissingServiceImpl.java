package com.talaash.service;

import com.talaash.dao.MissingCaseDao;
import com.talaash.dao.UserDao;
import com.talaash.interfaces.ReportMissingService;
import com.talaash.model.MissingCase;
import com.talaash.model.User;
import com.talaash.translator.Translator;
import com.talaash.translator.Utils;
import com.talaash.vo.CreateMissingCaseResponse;
import com.talaash.vo.MissingCaseRequest;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by imittal on 5/18/16.
 */
@Service
public class ReportMissingServiceImpl implements ReportMissingService {

    @Autowired
    MissingCaseDao missingCaseDao;

    @Autowired
    UserDao userDao;

    @Autowired
    Translator<MissingCase, MissingCaseRequest, String> translator;

    @Override
    public CreateMissingCaseResponse createNewCase(MissingCaseRequest reportMissingRequest, String userToken) {
        if (reportMissingRequest.getImageEncodedString() != null) {
            try {
                System.out.println("hi");
                byte[] imageByteArray = Base64.decodeBase64(reportMissingRequest.getImageEncodedString());
                Path pathToFile = Paths.get(Utils.setListOfMissingCaseImagesPaths(1, userToken).get(0));
                Files.createDirectories(pathToFile.getParent());
                Files.createFile(pathToFile);
                FileOutputStream imageOutFile = new FileOutputStream(Utils.setListOfMissingCaseImagesPaths(1, userToken).get(0));
                imageOutFile.write(imageByteArray);
                imageOutFile.close();
                System.out.println("hi2");
                System.out.println("Image Successfully Stored");
            } catch (FileNotFoundException fnfe) {
                System.out.println("Image Path not found" + fnfe);
                return new CreateMissingCaseResponse(true, 0);
            } catch (IOException ioe) {
                System.out.println("Exception while converting the Image " + ioe);
                return new CreateMissingCaseResponse(true, 0);
            } catch (Exception e) {
                System.out.println("Unable to uplaod image. No image exists");
                return new CreateMissingCaseResponse(true, 0);
            }
        }
        User user = userDao.findByUserToken(userToken);
        reportMissingRequest.setUser(user);
        MissingCase newCase = missingCaseDao.save(translator.translate(reportMissingRequest, userToken));
        return new CreateMissingCaseResponse(false, newCase.getId());
    }
}
