package com.talaash.service;

import com.talaash.dao.MissingAndFoundMappingDao;
import com.talaash.dao.MissingCaseDao;
import com.talaash.interfaces.CaseService;
import com.talaash.model.FoundCase;
import com.talaash.model.MissingCase;
import com.talaash.translator.Translator;
import com.talaash.vo.CaseResponse;
import com.talaash.vo.CloseCaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by imittal on 5/18/16.
 */
@Service
public class CaseServiceImpl implements CaseService {

    @Autowired
    private MissingCaseDao missingCaseDao;

    @Autowired
    private MissingAndFoundMappingDao missingAndFoundMappingDao;

    @Autowired
    private Translator<CaseResponse, List<MissingCase>, List<FoundCase>> translator;

    @Override
    public CaseResponse getMyCase(String userToken) {
        List<MissingCase> missingCases = missingCaseDao.getMyCase(userToken);
        System.out.println(missingCases.size());
        List<FoundCase> foundCases = missingAndFoundMappingDao.getMyCaseStatuses(userToken);
        System.out.println(foundCases.size());
        return translator.translate(missingCases, foundCases);
    }

    @Override
    public CloseCaseResponse closeCase(String userToken) {
        missingCaseDao.updateCaseStatus(true, userToken);
        return new CloseCaseResponse(false);
    }

    @Override
    public CaseResponse getCaseById(int id) {
        List<MissingCase> missingCases = missingCaseDao.findById(id);
        List<FoundCase> foundCases = missingAndFoundMappingDao.getCaseStatuses(id);
        return translator.translate(missingCases, foundCases);
    }
}
