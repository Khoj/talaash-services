package com.talaash.service;

import com.talaash.interfaces.NewsFeedService;
import com.talaash.translator.Translator;
import com.talaash.vo.Example;
import com.talaash.vo.FeedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subha_000 on 04-06-2016.
 */
@Component
public class NewsFeedServiceImpl implements NewsFeedService {
    private RestTemplate restTemplate = new RestTemplate();

    @Autowired
    private Translator<List<FeedResponse>, Example, String> translator;

    @Override
    public List<FeedResponse> getFeeds(String userToken) {
        List<FeedResponse> feedResponseList = getFeedsFromFacebook();
        return feedResponseList;

    }

    private List<FeedResponse> getFeedsFromFacebook() {
        String mainUrl = "https://graph.facebook.com/v2.6/Missing-people-India/posts?access_token=182067478807123|ekdK5BDxsdQwcm9ltr-I4vSfa7Y&fields=description,full_picture,name,created_time";
        Example example = restTemplate.getForObject(mainUrl, Example.class);
        List<FeedResponse> allFeeds = translator.translate(example, "");
        List<FeedResponse> filteredFeeds = getFilteredFeeds(allFeeds);
        return filteredFeeds;
    }

    private List<FeedResponse> getFilteredFeeds(List<FeedResponse> allFeeds) {
        List<FeedResponse> filteredFeeds = new ArrayList<FeedResponse>();
        for (FeedResponse feed : allFeeds) {
            if (feed.getDescription() != null
                    & feed.getImageUrl() != null
                    & feed.getName() != null
                    & feed.getPostedOn() != null)
                filteredFeeds.add(feed);
        }
        return filteredFeeds;
    }


}
