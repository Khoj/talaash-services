package com.talaash.service;

import com.talaash.dao.UserDao;
import com.talaash.interfaces.LoginService;
import com.talaash.model.User;
import com.talaash.vo.LogInResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by imittal on 5/12/16.
 */
@Service
public class LoginServiceImpl implements LoginService {

    @Autowired
    UserDao userDao;

    @Override
    public LogInResponse performLogin(String username, String password) {

        for (User user : userDao.findAll()) {
            if (user.getUserName().equals(username) && user.getPassword().equals(password)) {
                return new LogInResponse(user.getUserToken(), false);
            }
        }
        return new LogInResponse("", true);
    }
}
