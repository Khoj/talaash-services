package com.talaash.service;

import com.talaash.dao.FoundCaseDao;
import com.talaash.dao.MissingAndFoundMappingDao;
import com.talaash.dao.MissingCaseDao;
import com.talaash.dao.UserDao;
import com.talaash.interfaces.ReportFoundService;
import com.talaash.model.FoundCase;
import com.talaash.model.MissingAndFoundMapping;
import com.talaash.model.MissingCase;
import com.talaash.model.User;
import com.talaash.translator.Translator;
import com.talaash.translator.Utils;
import com.talaash.vo.ReportFoundRequest;
import com.talaash.vo.ReportFoundResponse;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by imittal on 5/18/16.
 */
@Service
public class ReportFoundServiceImpl implements ReportFoundService {

    @Autowired
    private FoundCaseDao foundCaseDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private MissingCaseDao missingCaseDao;
    @Autowired
    private MissingAndFoundMappingDao missingAndFoundMappingDao;
    @Autowired
    private Translator<FoundCase, ReportFoundRequest, String> translator;

    @Override
    public ReportFoundResponse reportFoundPerson(ReportFoundRequest reportFoundRequest, String userToken) {
        if (reportFoundRequest.getImageEncodedString() != null) {
            try {
                byte[] imageByteArray = Base64.decodeBase64(reportFoundRequest.getImageEncodedString());
                Path pathToFile = Paths.get(Utils.setListOfFoundCaseImagesPaths(1, userToken).get(0));
                Files.createDirectories(pathToFile.getParent());
                Files.createFile(pathToFile);
                FileOutputStream imageOutFile = new FileOutputStream(Utils.setListOfFoundCaseImagesPaths(1, userToken).get(0));
                imageOutFile.write(imageByteArray);
                imageOutFile.close();
                System.out.println("Image Successfully Stored");
            } catch (FileNotFoundException fnfe) {
                System.out.println("Image Path not found" + fnfe);
                return new ReportFoundResponse(true);
            } catch (IOException ioe) {
                System.out.println("Exception while converting the Image " + ioe);
                return new ReportFoundResponse(true);
            } catch (Exception e) {
                return new ReportFoundResponse(true);
            }
        }
        User user = userDao.findByUserToken(userToken);
        List<MissingCase> missingCaseList = missingCaseDao.findById(reportFoundRequest.getMissingCaseId());
        reportFoundRequest.setUser(user);
        FoundCase foundCase = foundCaseDao.save(translator.translate(reportFoundRequest, userToken));
        missingAndFoundMappingDao.save(new MissingAndFoundMapping(missingCaseList.get(0), foundCase));
        System.out.println("HER 2");
        return new ReportFoundResponse(false);
    }
}
