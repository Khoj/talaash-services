package com.talaash.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * Created by imittal on 5/18/16.
 */
@Entity
@Table(name = "missing_found")
public class MissingAndFoundMapping implements Serializable {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "id")
    private int id;
    @NotNull
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "missing_case_id")
    private MissingCase missingCase;
    @NotNull
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "found_case_id")
    private FoundCase foundCase;

    public MissingAndFoundMapping(MissingCase missingCase, FoundCase foundCase) {
        this.missingCase = missingCase;
        this.foundCase = foundCase;
    }

    public MissingAndFoundMapping() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public MissingCase getMissingCase() {
        return missingCase;
    }

    public void setMissingCase(MissingCase missingCase) {
        this.missingCase = missingCase;
    }

    public FoundCase getFoundCase() {
        return foundCase;
    }

    public void setFoundCase(FoundCase foundCase) {
        this.foundCase = foundCase;
    }
}
