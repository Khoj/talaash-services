package com.talaash.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * Created by imittal on 5/15/16.
 */
@Entity
@Table(name = "missing_case")
public class MissingCase implements Serializable {

    @Id
    @GeneratedValue(generator = "increment")
    @GenericGenerator(name = "increment", strategy = "increment")
    @Column(name = "missing_case_id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private String age;
    @Column(name = "height")
    private String height;
    @Column(name = "height_unit")
    private String heightUnit;
    @Column(name = "identification_mark_one")
    private String identificationMarkOne;
    @Column(name = "identification_mark_two")
    private String identificationMarkTwo;
    @Column(name = "gender")
    private String gender;
    @Column(name = "language_one")
    private String languageOne;
    @Column(name = "language_two")
    private String languageTwo;
    @Column(name = "language_three")
    private String languageThree;
    @Column(name = "language_four")
    private String languageFour;
    @Column(name = "last_located_at")
    private String lastLocatedAt;
    @Column(name = "relation_to_missing_person")
    private String relationToMissingPerson;
    @Column(name = "details")
    private String details;
    @Column(name = "other_details")
    private String otherDetails;
    @Column(name = "date")
    private Date date;
    @Column(name = "time")
    private Timestamp time;
    @Column(name = "no_of_images")
    private int numberOfPics;
    @Column(name = "status_of_case")
    private Boolean statusOfCase;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getNumberOfPics() {
        return numberOfPics;
    }

    public void setNumberOfPics(int numberOfPics) {
        this.numberOfPics = numberOfPics;
    }

    public Boolean getStatusOfCase() {
        return statusOfCase;
    }

    public void setStatusOfCase(Boolean statusOfCase) {
        this.statusOfCase = statusOfCase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public String getIdentificationMarkOne() {
        return identificationMarkOne;
    }

    public void setIdentificationMarkOne(String identificationMarkOne) {
        this.identificationMarkOne = identificationMarkOne;
    }

    public String getIdentificationMarkTwo() {
        return identificationMarkTwo;
    }

    public void setIdentificationMarkTwo(String identificationMarkTwo) {
        this.identificationMarkTwo = identificationMarkTwo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLanguageOne() {
        return languageOne;
    }

    public void setLanguageOne(String languageOne) {
        this.languageOne = languageOne;
    }

    public String getLanguageTwo() {
        return languageTwo;
    }

    public void setLanguageTwo(String languageTwo) {
        this.languageTwo = languageTwo;
    }

    public String getLanguageThree() {
        return languageThree;
    }

    public void setLanguageThree(String languageThree) {
        this.languageThree = languageThree;
    }

    public String getLanguageFour() {
        return languageFour;
    }

    public void setLanguageFour(String languageFour) {
        this.languageFour = languageFour;
    }

    public String getLastLocatedAt() {
        return lastLocatedAt;
    }

    public void setLastLocatedAt(String lastLocatedAt) {
        this.lastLocatedAt = lastLocatedAt;
    }

    public String getRelationToMissingPerson() {
        return relationToMissingPerson;
    }

    public void setRelationToMissingPerson(String relationToMissingPerson) {
        this.relationToMissingPerson = relationToMissingPerson;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
