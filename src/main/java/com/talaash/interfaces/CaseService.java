package com.talaash.interfaces;

import com.talaash.vo.CaseResponse;
import com.talaash.vo.CloseCaseResponse;

/**
 * Created by imittal on 5/18/16.
 */
public interface CaseService {

    CaseResponse getMyCase(String userToken);

    CloseCaseResponse closeCase(String userToken);

    CaseResponse getCaseById(int id);
}
