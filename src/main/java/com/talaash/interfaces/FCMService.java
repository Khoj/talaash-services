package com.talaash.interfaces;

import com.talaash.vo.TokenRegistrationResponse;

/**
 * Created by imittal on 6/3/16.
 */
public interface FCMService {
    TokenRegistrationResponse registerMyDevice(String token);
}
