package com.talaash.interfaces;

import com.talaash.vo.CreateMissingCaseResponse;
import com.talaash.vo.MissingCaseRequest;

/**
 * Created by imittal on 5/18/16.
 */
public interface ReportMissingService {

    CreateMissingCaseResponse createNewCase(MissingCaseRequest reportMissingRequest, String userToken);
}
