package com.talaash.interfaces;

import com.talaash.vo.GenerateOtpResponse;
import com.talaash.vo.OtpResponse;

/**
 * Created by imittal on 5/15/16.
 */
public interface SignUpService {

    GenerateOtpResponse generateOtp(String phoneNumber, String username);

    OtpResponse verifyOtp(String userName, String password, String phoneNumber, String correctOtp, String enteredOtp);
}
