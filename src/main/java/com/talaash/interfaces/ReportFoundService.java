package com.talaash.interfaces;

import com.talaash.vo.ReportFoundRequest;
import com.talaash.vo.ReportFoundResponse;

/**
 * Created by imittal on 5/18/16.
 */
public interface ReportFoundService {

    ReportFoundResponse reportFoundPerson(ReportFoundRequest reportFoundRequest, String userToken);
}
