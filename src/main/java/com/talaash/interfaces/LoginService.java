package com.talaash.interfaces;

import com.talaash.vo.LogInResponse;

/**
 * Created by imittal on 5/12/16.
 */
public interface LoginService {

    LogInResponse performLogin(String username, String password);
}
