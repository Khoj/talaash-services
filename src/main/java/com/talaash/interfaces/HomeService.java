package com.talaash.interfaces;

import com.talaash.vo.MissingAndFoundResponse;

import java.util.List;

/**
 * Created by imittal on 5/17/16.
 */
public interface HomeService {

    List<MissingAndFoundResponse> getMissingPeople(String userToken);

    List<MissingAndFoundResponse> getFoundPeople(String userToken);
}
