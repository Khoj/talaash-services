package com.talaash.interfaces;

import com.talaash.vo.FeedResponse;

import java.util.List;

/**
 * Created by subha_000 on 04-06-2016.
 */
public interface NewsFeedService {

    List<FeedResponse> getFeeds(String userToken);
}
