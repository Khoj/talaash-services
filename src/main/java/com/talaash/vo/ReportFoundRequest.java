package com.talaash.vo;

import com.talaash.model.User;

/**
 * Created by imittal on 5/15/16.
 */
public class ReportFoundRequest {
    private String date;
    private String time;
    private String approxAge;
    private String approxHeight;
    private String identificationMarkOne;
    private String identificationMarkTwo;
    private String otherDetails;
    private String heightUnit;
    private int numberOfPics;
    private String imageEncodedString;
    private User user;
    private String name;
    private String lastLocatedAt;
    private String gender;
    private int missingCaseId;

    public int getMissingCaseId() {
        return missingCaseId;
    }

    public void setMissingCaseId(int missingCaseId) {
        this.missingCaseId = missingCaseId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getLastLocatedAt() {
        return lastLocatedAt;
    }

    public void setLastLocatedAt(String lastLocatedAt) {
        this.lastLocatedAt = lastLocatedAt;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeightUnit() {
        return heightUnit;
    }

    public void setHeightUnit(String heightUnit) {
        this.heightUnit = heightUnit;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public int getNumberOfPics() {
        return numberOfPics;
    }

    public void setNumberOfPics(int numberOfPics) {
        this.numberOfPics = numberOfPics;
    }

    public String getImageEncodedString() {
        return imageEncodedString;
    }

    public void setImageEncodedString(String imageEncodedString) {
        this.imageEncodedString = imageEncodedString;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getApproxAge() {
        return approxAge;
    }

    public void setApproxAge(String approxAge) {
        this.approxAge = approxAge;
    }

    public String getApproxHeight() {
        return approxHeight;
    }

    public void setApproxHeight(String approxHeight) {
        this.approxHeight = approxHeight;
    }

    public String getIdentificationMarkOne() {
        return identificationMarkOne;
    }

    public void setIdentificationMarkOne(String identificationMarkOne) {
        this.identificationMarkOne = identificationMarkOne;
    }

    public String getIdentificationMarkTwo() {
        return identificationMarkTwo;
    }

    public void setIdentificationMarkTwo(String identificationMarkTwo) {
        this.identificationMarkTwo = identificationMarkTwo;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }
}
