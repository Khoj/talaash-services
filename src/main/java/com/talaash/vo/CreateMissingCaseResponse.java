package com.talaash.vo;

/**
 * Created by imittal on 5/18/16.
 */
public class CreateMissingCaseResponse {
    private boolean error;
    private int caseId;

    public CreateMissingCaseResponse(boolean error, int caseId) {
        this.error = error;
        this.caseId = caseId;
    }

    public int getCaseId() {
        return caseId;
    }

    public void setCaseId(int caseId) {
        this.caseId = caseId;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
