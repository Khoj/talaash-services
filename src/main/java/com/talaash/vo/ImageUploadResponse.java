package com.talaash.vo;

/**
 * Created by imittal on 5/21/16.
 */
public class ImageUploadResponse {

    private boolean error;

    public ImageUploadResponse(boolean error) {
        this.error = error;
    }

    public ImageUploadResponse() {
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
