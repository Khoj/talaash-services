package com.talaash.vo;

/**
 * Created by imittal on 5/20/16.
 */
public class GenerateOtpRequest {
    private String phoneNumber;
    private String userName;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
