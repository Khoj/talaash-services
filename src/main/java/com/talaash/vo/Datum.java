package com.talaash.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by subha_000 on 04-06-2016.
 */
public class Datum {

    private String fullPicture;
    private String name;
    private String id;
    private String description;
    private String createdTime;


    /**
     * @return The fullPicture
     */
    public String getFullPicture() {
        return fullPicture;
    }

    /**
     * @param fullPicture The full_picture
     */
    @JsonProperty("full_picture")
    public void setFullPicture(String fullPicture) {
        this.fullPicture = fullPicture;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    public String getCreatedTime() {
        return createdTime;
    }

    @JsonProperty("created_time")
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }
}
