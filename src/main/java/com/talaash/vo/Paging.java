package com.talaash.vo;

/**
 * Created by subha_000 on 04-06-2016.
 */
public class Paging {
    private String previous;
    private String next;

    @Override
    public String toString() {
        return "Paging{" +
                "previous='" + previous + '\'' +
                ", next='" + next + '\'' +
                '}';
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }
}
