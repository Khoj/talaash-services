package com.talaash.vo;

/**
 * Created by imittal on 6/3/16.
 */
public class TokenRegistrationResponse {
    private boolean error;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
