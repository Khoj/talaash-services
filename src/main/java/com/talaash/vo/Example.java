package com.talaash.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by subha_000 on 04-06-2016.
 */
public class Example {

    private List<Datum> data = new ArrayList<Datum>();

    private Paging paging;

    @Override
    public String toString() {
        return "Example{" +
                "data=" + data +
                ", paging=" + paging +
                '}';
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }
}
