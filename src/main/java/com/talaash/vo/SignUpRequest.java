package com.talaash.vo;

/**
 * Created by imittal on 5/15/16.
 */
public class SignUpRequest {
    private String phoneNumber;
    private String userName;
    private String password;
    private String correctOtp;
    private String enteredOtp;

    public String getEnteredOtp() {
        return enteredOtp;
    }

    public void setEnteredOtp(String enteredOtp) {
        this.enteredOtp = enteredOtp;
    }

    public String getCorrectOtp() {
        return correctOtp;
    }

    public void setCorrectOtp(String correctOtp) {
        this.correctOtp = correctOtp;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
