package com.talaash.controller;

import com.talaash.interfaces.SignUpService;
import com.talaash.vo.GenerateOtpRequest;
import com.talaash.vo.GenerateOtpResponse;
import com.talaash.vo.OtpResponse;
import com.talaash.vo.SignUpRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by imittal on 5/15/16.
 */
@RestController
@RequestMapping("/signup")
public class SignUpController {

    @Autowired
    SignUpService signUpService;

    @RequestMapping(value = "/generateOtp", method = RequestMethod.POST)
    public GenerateOtpResponse generateOtp(@RequestBody GenerateOtpRequest generateOtpRequest) {
        System.out.println("reached here 1");
        return signUpService.generateOtp(generateOtpRequest.getPhoneNumber(), generateOtpRequest.getUserName());
    }

    @RequestMapping(value = "/verifyOtp", method = RequestMethod.POST)
    public ResponseEntity<OtpResponse> verifyOtp(@RequestBody SignUpRequest request) {
        return new ResponseEntity<OtpResponse>(signUpService.verifyOtp(request.getUserName(), request.getPassword(), request.getPhoneNumber(), request.getCorrectOtp(), request.getEnteredOtp()), HttpStatus.OK);
    }
}
