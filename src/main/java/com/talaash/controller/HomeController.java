package com.talaash.controller;

import com.talaash.interfaces.HomeService;
import com.talaash.vo.MissingAndFoundResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by imittal on 5/17/16.
 */
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeService homeService;

    @RequestMapping(value = "/getmissing", method = RequestMethod.POST)
    public ResponseEntity<List<MissingAndFoundResponse>> getMissingPeople(@RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<>(homeService.getMissingPeople(userToken), HttpStatus.OK);
    }

    @RequestMapping(value = "/getfound", method = RequestMethod.POST)
    public ResponseEntity<List<MissingAndFoundResponse>> getFoundPeople(@RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<>(homeService.getFoundPeople(userToken), HttpStatus.OK);
    }

}
