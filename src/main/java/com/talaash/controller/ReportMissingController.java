package com.talaash.controller;

import com.talaash.interfaces.ReportMissingService;
import com.talaash.vo.CreateMissingCaseResponse;
import com.talaash.vo.MissingCaseRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by imittal on 5/15/16.
 */
@RestController
@RequestMapping("/newcase")
public class ReportMissingController {

    @Autowired
    ReportMissingService reportMissingService;

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public CreateMissingCaseResponse createNewCase(@RequestBody MissingCaseRequest reportMissingRequest, @RequestHeader("User-Token") String userToken) {
        return reportMissingService.createNewCase(reportMissingRequest, userToken);
    }

    /*@RequestMapping(value="/upload",method = RequestMethod.POST)
    public ImageUploadResponse uploadImage(@RequestBody ImageUpload imageUpload){
        return null;
    }*/
}
