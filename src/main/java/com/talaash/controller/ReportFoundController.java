package com.talaash.controller;

import com.talaash.interfaces.ReportFoundService;
import com.talaash.vo.ReportFoundRequest;
import com.talaash.vo.ReportFoundResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by imittal on 5/15/16.
 */
@RestController
@RequestMapping("/found")
public class ReportFoundController {

    @Autowired
    ReportFoundService reportFoundService;

    @RequestMapping(value = "/report", method = RequestMethod.POST)
    public ResponseEntity<ReportFoundResponse> reportFound(@RequestBody ReportFoundRequest reportFoundRequest, @RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<ReportFoundResponse>(reportFoundService.reportFoundPerson(reportFoundRequest, userToken), HttpStatus.OK);
    }

}
