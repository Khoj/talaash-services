package com.talaash.controller;

import com.talaash.interfaces.CaseService;
import com.talaash.vo.CaseResponse;
import com.talaash.vo.CloseCaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by imittal on 5/15/16.
 */
@RestController
@RequestMapping("/case")
public class CaseController {

    @Autowired
    CaseService myCaseService;

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public ResponseEntity<CaseResponse> getMyCase(@RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<CaseResponse>(myCaseService.getMyCase(userToken), HttpStatus.OK);
    }


    @RequestMapping(value = "/close", method = RequestMethod.POST)
    public ResponseEntity<CloseCaseResponse> closeMyCase(@RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<CloseCaseResponse>(myCaseService.closeCase(userToken), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<CaseResponse> getCaseById(@PathVariable int id) {
        return new ResponseEntity<CaseResponse>(myCaseService.getCaseById(id), HttpStatus.OK);
    }

}
