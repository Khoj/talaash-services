package com.talaash.controller;

import com.talaash.interfaces.NewsFeedService;
import com.talaash.vo.FeedResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by subha_000 on 04-06-2016.
 */

@RestController
@RequestMapping("/feeds")
public class FeedController {


    @Autowired
    NewsFeedService newsFeedService;

    @RequestMapping(value = "/get", method = RequestMethod.POST)
    public ResponseEntity<List<FeedResponse>> getFeeds(@RequestHeader("User-Token") String userToken) {
        return new ResponseEntity<>(newsFeedService.getFeeds(userToken), HttpStatus.OK);
    }
}
