package com.talaash.controller;

import com.talaash.interfaces.LoginService;
import com.talaash.model.User;
import com.talaash.vo.LogInResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by imittal on 5/12/16.
 */
@RestController
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private LoginService loginService;

    @RequestMapping(value = "/hello")
    public String hi() {
        return "Hello";
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    public ResponseEntity<LogInResponse> authenticateUser(@RequestBody User request) {
        return new ResponseEntity<>(loginService.performLogin(request.getUserName(), request.getPassword()), HttpStatus.OK);
    }
}
